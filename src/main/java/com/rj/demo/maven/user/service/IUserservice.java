package com.rj.demo.maven.user.service;

import com.rj.demo.maven.user.entity.User;

/**
 * Hello world!
 *
 */
public interface IUserservice
{
	
	public int deleteByPrimaryKey(Integer id)throws Exception;
	
	public int insert(User record)throws Exception;
	
	public int insertSelective(User record)throws Exception;
	
	public User queryByPrimaryKey(String userId)throws Exception;
	
	public int updateByPrimaryKeySelective(User record)throws Exception;
	
	public int updateByPrimaryKey(User record)throws Exception;
	
}
