package com.rj.demo.maven.user.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rj.demo.maven.user.entity.User;
import com.rj.demo.maven.user.mapper.UserMapper;
import com.rj.demo.maven.user.service.IUserservice;

@Service
public class UserserviceImpl implements IUserservice {
	
	@Autowired
	private UserMapper userMapper;

	@Override
	public int deleteByPrimaryKey(Integer id) throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insert(User record)throws Exception {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int insertSelective(User record) throws Exception{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public User queryByPrimaryKey(String userId) throws Exception{
		User user = userMapper.selectByPrimaryKey(userId);
		return user;
	}

	@Override
	public int updateByPrimaryKeySelective(User record)throws Exception{
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int updateByPrimaryKey(User record) throws Exception{
		// TODO Auto-generated method stub
		return 0;
	}

}
