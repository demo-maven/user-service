package com.rj.demo.maven.user.user.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.rj.demo.maven.user.entity.User;
import com.rj.demo.maven.user.service.IUserservice;



@RunWith(SpringJUnit4ClassRunner.class)
//告诉junit spring配置文件的位置
@ContextConfiguration(value = {"classpath:spring/spring-service.xml","classpath:spring/spring-transaction.xml"})
public class BaseTest {
  
	@Autowired
	private IUserservice userService;
	
    @Test
    public void testApp() {
    	String userId = "340219586710339584";
        User user;
		try {
			user = userService.queryByPrimaryKey(userId);
			System.out.println(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
    }
}
